/**
 * @author sunil_s
 * @created on 12/18/2021
 * @description Step Definition file for Apply Coupon Feature
 */
package de.payback.client.android.app.stepDef;

import org.openqa.selenium.WebDriver;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.payback.client.android.app.pages.CouponsScreen;
import de.payback.client.android.app.pages.HomeScreen;
import de.payback.client.android.app.pages.LoginScreen;
import de.payback.client.android.app.scenarioTests.PaybackAppTest;
import de.payback.client.android.app.utils.AppUtils;

public class ApplyCoupon {

	private static LoginScreen loginscreen;
	private static HomeScreen homescreen;
	private static CouponsScreen couponsscreen;
	String partner;
	AppUtils commonUtils;
	WebDriver driver;

	@Before
	public void init() {
		driver = PaybackAppTest.driver;
	
		loginscreen = new LoginScreen(driver);
		homescreen = new HomeScreen(driver);
		couponsscreen = new CouponsScreen(driver);	
	}

	@Given("^Launch Payback app on device$")
	public void launch_Payback_app_on_device() throws Throwable {
		loginscreen.validateLoginScreen();
		loginscreen.clickOnLoginBtn();
	}

	@When("^Login with credentials$")
	public void login_with_credentials(DataTable loginCredentials) throws Throwable {
		loginscreen.enterLoginCredentials(loginCredentials);
	}

	@Then("^user login should be successfully$")
	public void user_login_should_be_successfully() throws Throwable {
		homescreen.validateHomeScreen();
	}

	@When("^click Coupons on home screen$")
	public void click_Coupons_on_home_screen() throws Throwable {
		homescreen.clickOnCoupons();
	}

	@Then("^navigate to Coupons screen$")
	public void navigate_to_Coupons_screen() throws Throwable {
		couponsscreen.validateCouponsScreen();
	}

	@When("^click on Filters to apply filter$")
	public void click_on_Filters_to_apply_filter() throws Throwable {
		couponsscreen.clickOnFilters();
	}

	@When("^select \"([^\"]*)\" to apply Partner filter$")
	public void select_to_apply_Partner_filter(String partner) throws Throwable {
		couponsscreen.validateFilterScreen();
		this.partner = partner.toUpperCase();
		couponsscreen.selectPartnerFilterAndApply();
		
	}

	@Then("^coupons based on applied filter should display$")
	public void coupons_based_on_applied_filter_should_display() throws Throwable {
		couponsscreen.validateCouponsScreenAfterFilter(partner);
	}


	@When("^click to activate the first coupon$")
	public void click_to_activate_the_first_coupon() throws Throwable {
	   couponsscreen.clickToActivateCoupon();
	}

	@Then("^coupon should be successfully activated$")
	public void coupon_should_be_successfully_activated() throws Throwable {
		couponsscreen.validateCouponActivation();
	}

	@When("^navigate to user menu and click on logout$")
	public void navigated_to_user_menu_and_click_on_logout() throws Throwable {
		homescreen.clickOnAktuell();
		homescreen.logout();
	}
	
	@Then("^user should logged out successfully$")
	public void user_should_logged_out_successfully() throws Throwable {
		loginscreen.validateLoginScreen();
	}

}
