/**
 * @author sunil_s
 * @created on 12/18/2021
 * @description Class to initiate Payback App Tests
 */
package de.payback.client.android.app.scenarioTests;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import de.payback.client.android.app.common.SingletonDriver;
import de.payback.client.android.app.utils.AppUtils;
import io.appium.java_client.remote.MobileCapabilityType;

@CucumberOptions(features = "./src/test/resources/AppFeature", glue = {
		"de.payback.client.android.app.stepDef" }, tags = {}, plugin = { "html:target/cucumber-html-report",
				"json:target/cucumber.json", "pretty:target/cucumber-pretty.txt", "junit:target/cucumber-results.xml" })
public class PaybackAppTest extends AbstractTestNGCucumberTests {

	public static WebDriver driver;

	public static String platformName;
	public static String deviceName;
	public static String platformVersion;
	public static String appPath;
	public static String logfilePath;

	DesiredCapabilities capabilities = new DesiredCapabilities();

	// Getter Method for Platform Variable
	public static String getPlatform() {
		return platformName;
	}

	// Setter Method for Platform Variable
	public static void setPlatform(String platformName) {
		PaybackAppTest.platformName = platformName;
	}

	// Getter Method for DeviceName Variable
	public static String getDeviceName() {
		return deviceName;
	}

	// Setter Method for DeviceName Variable
	public static void setDeviceName(String deviceName) {
		PaybackAppTest.deviceName = deviceName;
	}

	// Getter Method for Platform Version Variable
	public static String getPlatformVersion() {
		return platformVersion;
	}

	// Setter Method for Platform Version Variable
	public static void setPlatformVersion(String platformVersion) {
		PaybackAppTest.platformVersion = platformVersion;
	}

	// Getter Method for AppPath Variable
	public static String getAppPath() {
		return appPath;
	}

	// Setter Method for AppPath Variable
	public static void setAppPath(String appPath) {
		PaybackAppTest.appPath = appPath;
	}

	// Getter Method for LogFilePath Variable
	public static String getLogfilePath() {
		return logfilePath;
	}

	// Setter Method for LogfilePath Variable
	public static void setLogfilePath(String logfilePath) {
		PaybackAppTest.logfilePath = logfilePath;
	}

	
	@BeforeSuite(description = "Test Method to set capabilities required for Appium session")
	@Parameters({ "platform", "deviceName", "platformVersion", "automationName", "appPackage", "appActivity", "appPath",
			"logFilePath" })
	public void setUpAppium(String platform, String deviceName, String platformVersion, String automationName,
			String appPackage, String appActivity, String appPath, String logFilePath)
			throws IOException, InterruptedException {

		setPlatform(platform);
		setDeviceName(deviceName);
		setPlatformVersion(platformVersion);

		final String APPPATH = System.getProperty("user.dir") + appPath;
		setAppPath(APPPATH);

		final String LOGFILEPATH = System.getProperty("user.dir") + logfilePath;
		setLogfilePath(LOGFILEPATH);

		File app = new File(APPPATH); // File Path of APK file
		System.out.println(APPPATH);

		if (platform.equalsIgnoreCase("iOS")) {

			capabilities.setCapability("deviceName", deviceName);
			capabilities.setCapability("platformName", platform);
			capabilities.setCapability("platformVersion", platformVersion);
			capabilities.setCapability("app", app);
			capabilities.setCapability("automationName", automationName);
			capabilities.setCapability("newCommandTimeout", 280);
		} else {

			// Other capabilities
			capabilities.setCapability(MobileCapabilityType.APP, APPPATH);
			capabilities.setCapability("device", deviceName);

			// Mandatory Capabilities
			capabilities.setCapability("deviceName", deviceName);
			capabilities.setCapability("platformName", platform);

			capabilities.setCapability("appPackage", appPackage);
			capabilities.setCapability("appActivity", appActivity);
			capabilities.setCapability("automationName", automationName);
			capabilities.setCapability("noReset", "false");
			capabilities.setCapability("newCommandTimeout", 280);
			capabilities.setCapability("autoGrantPermissions", true);
			capabilities.setCapability("autoDismissAlerts", true);
			capabilities.setCapability("autoAcceptAlerts", true);

		}
	}

	
	@BeforeClass(description="Test Method to create a Appium Session")
	public void driverSet() throws IOException, InterruptedException {
		driver = SingletonDriver.getInstance(platformName, capabilities).getDriver();
	}

	
	@AfterMethod(description = "Test Method to capture logs on failure")
	public void tearDown(ITestResult result) throws Exception {
		try {
			if (!result.isSuccess()) {
				SimpleDateFormat formater = new SimpleDateFormat("ddMMyyyy_HHmmss");
				Calendar calendar = Calendar.getInstance();

				String classname1 = this.getClass().getName();
				int clsname2 = classname1.lastIndexOf('.') + 1;
				String className = classname1.substring(clsname2);

				String methodName = result.getName();

				String parameter = String.format("TestNG Debugger : %s() running with parameters %s.",
						result.getMethod().getConstructorOrMethod().getName(), Arrays.toString(result.getParameters()));
				System.out.println("parameter: " + parameter);
				String tcName = parameter.substring(parameter.indexOf("[") + 1, parameter.indexOf("[") + 6);

				// To capture the screenshot for the failure case(s)
				File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File((getLogfilePath() + className + "_" + methodName + "_" + tcName + "_"
						+ formater.format(calendar.getTime()) + ".png")));

				if ("ANDROID".equalsIgnoreCase(platformName)) {
					// To capture the logs for the failure case(s)
					System.out.println("Saving device log - start");
					@SuppressWarnings("deprecation")
					List<LogEntry> logEntries = driver.manage().logs().get("logcat").filter(Level.ALL);

					File logFile = new File(getLogfilePath() + className + "_" + methodName + "_" + tcName + "_"
							+ formater.format(calendar.getTime()) + ".txt");
					@SuppressWarnings("resource")
					PrintWriter log_file_writer = new PrintWriter(logFile);
					for (LogEntry logEntry : logEntries) {
						if (logEntry.getMessage().toLowerCase().contains("de.payback.client.android")) {
							log_file_writer.println(logEntry);
							log_file_writer.flush();
						}
					}
					System.out.println("Saving device log - finish");
				}

			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	
	@AfterClass(description="Test Method to kill Appium session")
	public void suiteTearDown() throws Exception {
		driver.quit();
	}

}
