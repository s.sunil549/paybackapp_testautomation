#Author: sunil_s
@ApplyCoupon
Feature: Apply Coupon Feature Test

  Background: 
    Given Launch Payback app on device
    When Login with credentials
      | 3274648177 | demoUser1 |
    Then user login should be successfully

  Scenario Outline: To serach for coupons based on partner Filters and apply coupons
    When click Coupons on home screen
    Then navigate to Coupons screen
    When click on Filters to apply filter
    And select "<partner>" to apply Partner filter
    Then coupons based on applied filter should display
    When click to activate the first coupon
    Then coupon should be successfully activated
    When navigate to user menu and click on logout
    Then user should logged out successfully

    Examples: 
      | partner |
      | Rewe    |
