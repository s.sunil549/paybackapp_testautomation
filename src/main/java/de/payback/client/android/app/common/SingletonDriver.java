package de.payback.client.android.app.common;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class SingletonDriver {

	private static SingletonDriver driverClass = null;
	private WebDriver driver;

	/**
	 * @author sunil_s
	 * @description Constructor to create single instance of Driver
	 * @param platform
	 * @param capabiities
	 */
	public SingletonDriver(String platform, Capabilities capabiities) {

		try {
			if (platform.equalsIgnoreCase("iOS")) {
				driver = new IOSDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabiities);
			} else {
				driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabiities);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author sunil_s
	 * @description Method to get instance Singleton Driver
	 * @param platform
	 * @param desiredcapabilities
	 * @return
	 */
	public static SingletonDriver getInstance(String platform, Capabilities desiredcapabilities) {
		if (driverClass == null) {
			driverClass = new SingletonDriver(platform, desiredcapabilities);
		}
		return driverClass;

	}

	/**
	 * @author sunil_s
	 * @description Method to return singleton driver instance
	 * @return
	 */
	public WebDriver getDriver() {
		return driver;
	}

}
