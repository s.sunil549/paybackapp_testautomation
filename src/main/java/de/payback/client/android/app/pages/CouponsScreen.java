package de.payback.client.android.app.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import de.payback.client.android.app.common.CommonCV;
import de.payback.client.android.app.utils.AppUtils;

public class CouponsScreen {

	private WebDriver driver;
	private int coupons;

	public CouponsScreen(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//android.view.ViewGroup[@resource-id='de.payback.client.android:id/toolbar']/android.widget.TextView[@text='Coupons']")
	private WebElement couponsTxt;

	@FindBy(xpath = "//android.widget.LinearLayout[contains(@content-desc,'Nicht aktiviert')]")
	private WebElement nichtActiviertLnk;

	@FindBy(xpath = "//android.widget.LinearLayout[contains(@content-desc,'Aktiviert')]")
	private WebElement aktiviertLnk;

	@FindBy(id = "de.payback.client.android:id/filter_button")
	private WebElement filterBtn;

	@FindBy(xpath = "//android.widget.TextView[@text='Filter']")
	private WebElement filterTxt;

	@FindBy(id = "de.payback.client.android:id/coupon_action_reset")
	private WebElement cancelBtn;

	@FindBy(id = "de.payback.client.android:id/done")
	private WebElement freitigBtn;

	@FindAll({
			@FindBy(xpath = "//android.widget.TextView[@resource-id='de.payback.client.android:id/headline']/../androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout") })
	private List<WebElement> partnerCouponsList;

	// Method to get WebElement of Applied filters
	public WebElement getFilters(String value) {
		return driver.findElement(By.xpath("//android.widget.Button[@text='" + value + "']"));
	}

	@FindBy(xpath = "//android.widget.Button[@resource-id='de.payback.client.android:id/chip' and @text='hinzufügen']")
	private WebElement hinzufugenTxt;

	@FindAll({ @FindBy(id = "de.payback.client.android:id/card") })
	private List<WebElement> couponCards;

	@FindAll({ @FindBy(id = "de.payback.client.android:id/partner_logo") })
	private List<WebElement> partnerLogo;

	@FindAll({ @FindBy(id = "de.payback.client.android:id/incentivation") })
	private List<WebElement> infoIcon;

	@FindAll({ @FindBy(id = "de.payback.client.android:id/info_icon") })
	private List<WebElement> pointsTxt;

	@FindAll({ @FindBy(id = "de.payback.client.android:id/not_activated_button") })
	private List<WebElement> jetztAktivierenBtn;

	@FindAll({ @FindBy(id = "de.payback.client.android:id/activated_icon") })
	private List<WebElement> activatedIcon;

	@FindAll({ @FindBy(id = "de.payback.client.android:id/redeem_offline_button") })
	private List<WebElement> vorOrtEinLosenBtn;

	@FindBy(xpath = "//android.widget.TextView[@text='Super!']")
	private WebElement superTxt;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Schon alle Coupons für diese')]")
	private WebElement noCouponsMsgTxt;

	// Method to validate coupons screen
	public void validateCouponsScreen() {

		AppUtils.visibilityOfElementByXapth(driver,
				"//android.view.ViewGroup[@resource-id='de.payback.client.android:id/toolbar']/android.widget.TextView[@text='Coupons']",
				25);

		Assert.assertTrue(couponsTxt.isDisplayed(), " Coupons Text is not displayed ");
		Assert.assertTrue(nichtActiviertLnk.isDisplayed(), " nichtActivert Link is not displayed ");
		Assert.assertTrue(aktiviertLnk.isDisplayed(), " Aktivert Link is not displayed ");
		Assert.assertTrue(filterBtn.isDisplayed(), " Filter Button is not displayed ");

		Reporter.log("Test Step -> Coupons screen validation success ", true);
	}

	// Method to click on Filter
	public void clickOnFilters() {
		filterBtn.click();
		Reporter.log("Test Step -> Click on filter button success ", true);
	}

	// Method to validate Filter screen
	public void validateFilterScreen() {

		AppUtils.visibilityOfElementByXapth(driver, "//android.widget.TextView[@text='Filter']", 25);

		Assert.assertTrue(cancelBtn.isDisplayed(), " ZURÜCKSETZEN button is not displayed ");
		Assert.assertTrue(freitigBtn.isDisplayed(), " Freitig Button is not displayed ");
		Assert.assertTrue((partnerCouponsList.size() != 0), " Aktivert Link is not displayed ");

		Reporter.log("Test Step -> Filter screen validation success ", true);

	}

	// Method to apply Partner Filter
	public void selectPartnerFilterAndApply() {
		partnerCouponsList.get(2).click(); AppUtils.waitForElementToLoad(3);
		Reporter.log("Test Step -> Selecting partner filter and applying success", true);
	}

	// Method to validate coupons screen after applyed filter
	public void validateCouponsScreenAfterFilter(String partner) {
		
		
		AppUtils.visibilityOfElementByXapth(driver, "//android.widget.Button[@text='" + partner + "']", 25);

		Assert.assertTrue(getFilters(partner).isDisplayed(), partner + " applied filter is not displayed");
		Assert.assertTrue(hinzufugenTxt.isDisplayed(), "Hinzufugen Text is not displayed ");
		this.coupons = couponCards.size();
		if (coupons != 0) {
			Assert.assertTrue(couponCards.get(0).isDisplayed(), " Card is not displayed ");
			Assert.assertTrue(partnerLogo.get(0).isDisplayed(), " Partner Logo is not displayed ");
			Assert.assertTrue(infoIcon.get(0).isDisplayed(), " Info Icon is not displayed ");
			Assert.assertTrue(pointsTxt.get(0).isDisplayed(), " Points Text is not displayed ");
			Assert.assertTrue(jetztAktivierenBtn.get(0).isDisplayed(), " jetztAktivieren Button is not displayed ");
		} else {

			Assert.assertEquals(superTxt.getText().trim(), CommonCV.superMsg);
			Assert.assertTrue(noCouponsMsgTxt.getText().trim().contains(CommonCV.noCouponsMsg));
		}

		Reporter.log("Test Step -> Coupons screen validation after applying filter success ", true);

	}

	// Method to activate coupon
	public void clickToActivateCoupon() {
		if (coupons != 0) {
			jetztAktivierenBtn.get(0).click(); AppUtils.waitForElementToLoad(2);
			Reporter.log("Test Step -> Click on coupon success ", true);

		} else {
			Reporter.log("Test Step -> No coupons available to apply ", true);
		}
	}

	// Method to verify coupon activation
	public void validateCouponActivation() {
		if (coupons != 0) {
			Assert.assertTrue(activatedIcon.size() == 1);
			Assert.assertTrue(vorOrtEinLosenBtn.size() == 1);

			Reporter.log("Test Step -> Coupon activate successfully ", true);
		}
	}

}
