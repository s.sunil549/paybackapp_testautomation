package de.payback.client.android.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import de.payback.client.android.app.utils.AppUtils;
import io.appium.java_client.android.AndroidDriver;

public class HomeScreen {

	private WebDriver driver;

	public HomeScreen(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "de.payback.client.android:id/action_my_account")
	private WebElement myAccountIcon;

	@FindBy(id = "de.payback.client.android:id/main_feed_dest")
	private WebElement aktuellBtn;

	@FindBy(id = "de.payback.client.android:id/coupon_nav_graph")
	private WebElement couponsBtn;

	@FindBy(id = "de.payback.client.android:id/main_card_selection_dest")
	private WebElement kartePayBtn;

	@FindBy(id = "de.payback.client.android:id/online_shopping_nav_graph")
	private WebElement onlineShopsBtn;

	@FindBy(id = "de.payback.client.android:id/service_nav_graph")
	private WebElement servicesBtn;

	@FindBy(id = "de.payback.client.android:id/more_btn_logout")
	private WebElement ausLoggenBtn;

	@FindBy(xpath = "//android.widget.Button[@resource-id='android:id/button2' and @text='Logout']")
	private WebElement logoutBtn;

	// Method to validate home screen
	public void validateHomeScreen() {

		AppUtils.visibilityOfElementById(driver, "de.payback.client.android:id/action_my_account", 25);
		AppUtils.waitForElementToLoad(3);
		((AndroidDriver<?>) driver).tap(1, 160, 150, 100);
		AppUtils.waitForElementToLoad(1);

		Assert.assertTrue(myAccountIcon.isDisplayed(), "my Account Icon is not displayed");
		Assert.assertTrue(aktuellBtn.isDisplayed(), "Aktuell Button is not displayed");
		Assert.assertTrue(couponsBtn.isDisplayed(), "Coupons Button is not displayed");
		Assert.assertTrue(kartePayBtn.isDisplayed(), "KartePay Button is not displayed");
		Assert.assertTrue(onlineShopsBtn.isDisplayed(), "OnlineShops Button is not displayed");
		Assert.assertTrue(servicesBtn.isDisplayed(), "Services Button is not displayed");

		Reporter.log("Test Step -> Home screen validation success ", true);

	}

	// Method to click on Coupons
	public void clickOnCoupons() {
		couponsBtn.click();
		Reporter.log("Test Step -> Click on Coupons button success ", true);
	}

	// Method to click on Aktuell
	public void clickOnAktuell() {
		aktuellBtn.click();AppUtils.waitForElementToLoad(2);
		Reporter.log("Test Step -> Click on Aktuel button success ", true);
	}

	// Method to logout
	public void logout() {
		myAccountIcon.click();AppUtils.waitForElementToLoad(2);
		ausLoggenBtn.click();AppUtils.waitForElementToLoad(2);
		logoutBtn.click();
		
		Reporter.log("Test Step -> Click on Logout button success", true);

	}

}
