package de.payback.client.android.app.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AppUtils {

	private static WebDriverWait wait;

	// Method to wait for an element to load
	public static void waitForElementToLoad(int i) {
		try {
			Thread.sleep(i * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Method to check for the visibililty of the element
	public static void visibilityOfElement(WebDriver dr, WebElement ele, int timeUnit) {
		wait = new WebDriverWait(dr, timeUnit);
		wait.until(ExpectedConditions.visibilityOf(ele));
	}

	// Method to check for the visibililty of the element by locator -xpath
	public static void visibilityOfElementByXapth(WebDriver dr, String locator, int timeUnit) {
		wait = new WebDriverWait(dr, timeUnit);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
	}

	// Method to check for the visibililty of the element by locator -id
	public static void visibilityOfElementById(WebDriver dr, String locator, int timeUnit) {
		wait = new WebDriverWait(dr, timeUnit);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
	}

}
