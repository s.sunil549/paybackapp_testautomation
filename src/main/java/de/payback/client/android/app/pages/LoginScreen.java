package de.payback.client.android.app.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import cucumber.api.DataTable;
import de.payback.client.android.app.utils.AppUtils;

public class LoginScreen  {

	private WebDriver driver;

	public LoginScreen(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "de.payback.client.android:id/welcome_loginbutton")
	private WebElement loginBtn;

	@FindBy(id = "de.payback.client.android:id/welcome_registerbutton")
	private WebElement registerBtn;

	@FindBy(id = "de.payback.client.android:id/welcome_imprintbutton")
	private WebElement impressumBtn;

	@FindBy(id = "de.payback.client.android:id/welcome_termsbutton")
	private WebElement agbBtn;

	@FindBy(id = "de.payback.client.android:id/welcome_dataprotectionbutton")
	private WebElement datenschutzBtn;
	
	@FindBy(id="de.payback.client.android:id/txtLoginCheckCardNumber")
	private WebElement kundennummerTxtFld;
	
	@FindBy(id="de.payback.client.android:id/progressbutton_btn")
	private WebElement weiterBtn;
	
	@FindBy(xpath="//android.widget.TextView[@text='Login']")
	private WebElement loginTxt;
	
	@FindBy(id="de.payback.client.android:id/login_password_field")
	private WebElement passwortTxtFld;
	
	@FindBy(id="de.payback.client.android:id/progressbutton_btn")
	private WebElement aufGehtsBtn;
	
	@FindBy(id="de.payback.client.android:id/login_customernumber")
	private WebElement kundennummerTxt;

	// Method to validate login screen
	public void validateLoginScreen() {
		
		AppUtils.visibilityOfElementById(driver, "de.payback.client.android:id/welcome_loginbutton", 25);

		Assert.assertTrue(loginBtn.isDisplayed(), "Login Button is not displayed");
		Assert.assertTrue(registerBtn.isDisplayed(), "Register Button is not displayed");
		Assert.assertTrue(impressumBtn.isDisplayed(), "Impressum Button is not displayed");
		Assert.assertTrue(agbBtn.isDisplayed(), "AGB Button is not displayed");
		Assert.assertTrue(datenschutzBtn.isDisplayed(), "Datenschutz Button is not displayed");

		Reporter.log("Test Step -> Login screen validation success ", true);
	}

	// Method to click on Login Button
	public void clickOnLoginBtn() {

		loginBtn.click();
		Reporter.log("Test Step -> Click on login button success ", true);
	}
	
	//Method to enter login credentials
	public void enterLoginCredentials(DataTable loginCredentials){
		
		List<List<String>> testData = loginCredentials.raw();
		String paybackCardNum = testData.get(0).get(0);
		String password = testData.get(0).get(1);
		
		AppUtils.visibilityOfElementByXapth(driver, "//android.widget.TextView[@text='Login']", 25);
		
		Assert.assertTrue(loginTxt.isDisplayed(), " Login text is displayed ");
		kundennummerTxtFld.sendKeys(paybackCardNum);
		Assert.assertTrue(weiterBtn.isEnabled(), " Weiter Button is enabled ");
		weiterBtn.click();
		
		AppUtils.visibilityOfElementById(driver, "de.payback.client.android:id/login_customernumber", 25);
		String value = kundennummerTxt.getText().trim();
		Assert.assertTrue(value.contains(paybackCardNum), " Payback Card Number is not displayed ");
		
		passwortTxtFld.sendKeys(password);
		Assert.assertTrue(aufGehtsBtn.isEnabled(), " Aug Geht's Button is enabled ");
		aufGehtsBtn.click();
		
		Reporter.log("Test Step -> User loggedin successfully ", true);
	}

}
