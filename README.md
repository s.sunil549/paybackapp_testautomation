Mobile App Automation Framework
=======================

### Objective

The objective of this **Proof of Concept (PoC)** is to provide a functional automation solution for testing business flows within the Payback Android Application Application. The sequence of activities to achieve the PoC objective is as follows:

    1. Test Automation Suite to automation the basci flow of installation of app, he/she logins and navigate to the Coupon Center, filters the coupons by partner (he/she preferered partner is REWE) and after that he/she activates the first enabled coupon
    

### Proposed Solution

Inline to the objective, a robust framework is designed which is combination Page Object Model, Data Driven & BDD Framework using various **Open Source Tools** such as

* Cucumber - Behaviour Driven Development Framework support for Gherkin format
* Appium - Open Source Tools for Cross Platform Mobile Testing
* Maven - Dependency Management / Build Tool
* TestNG - Test Annotation to organize test suite and parameterization
* Java8 Programming language

The components of framework is structed as shown in below image:

![Framework](/src/main/resources/Screenshots/Framework.png)


### Installations

1. Download and Install JDK8 from the below link
    [https://adoptopenjdk.net/]
    
2. Download and Install Android Studio from the below link
  [https://developer.android.com/studio?gclid=CjwKCAjww-CGBhALEiwAQzWxOhsAUZlMWuodNM3WgYZUY82q1QILJoGWT2AW5ESns35NwSgD60pYfxoCjkwQAvD_BwE&gclsrc=aw.ds]

3.  Download and Install Appium Desktop from the below link 
     [https://github.com/appium/appium-desktop/releases/]

>Note: Appium version above  1.17.0 is preferrable, for this project have used 1.21.0

4. Download and Install Maven for Commandline from the below link
    [https://maven.apache.org/download.cgi]
    
5. Setting Envirnoment variables JAVA_HOME & M2_HOME

6. Download any IDE (Eclipse or IntelliJ) and install on machine.

7. Install Cucumber Plugin in Eclipse as shown in below link
    [https://github.com/cucumber/cucumber-eclipse-update-site-snapshot]
    
8. Download & Install Genymotion Tool for Creating Virtual Mobile Devices
    [https://www.genymotion.com/download/]

### Steps for Creating and Launhing Emulator 

Launching of Emulator can be done in two ways. 

1. Using Android Studio -> AVD Manager
    For detail information, please refer link [https://developer.android.com/studio/run/managing-avds]
    
2. Using Genymotion
    For detail information, please refer link [https://docs.genymotion.com/desktop/latest/Get_started/014_Basic_steps.html]

### Steps for Execution

1. Clone  the repository into any IDE ( Eclipse or IntelliJ )

2. Build the Project using **Maven** to update the dependencies

3. Run the below command from project directory to start execution

For running, App Suite
``` 
    mvn clean install -Drelease.arguments=Android_AppSuite.xml 
```


### Reports after execution

After execution, reports will be generated at **target -> cucumber-reports -> cucumber-html-reports**
and will be as below:

![ReportDetail](/src/main/resources/Screenshots/ReportSummary.png)

![ReportDetail1](/src/main/resources/Screenshots/ReportSummary1.png)

![Report](/src/main/resources/Screenshots/ReportDetail.png)
 







